//
//  CadastroViewController.swift
//  MyTask
//
//  Created by Bruno Ribeiro on 12/18/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit

class CadastroViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textNome: UITextField!
   
    @IBOutlet weak var textSobrenome: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
   
    @IBOutlet weak var txtSenha: UITextField!
    
    @IBOutlet weak var txtConfirmaSenha: UITextField!
    
    @IBOutlet weak var errorMessage: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        textNome.delegate = self
        textSobrenome.delegate = self
        txtEmail.delegate = self
        txtSenha.delegate = self
        txtConfirmaSenha.delegate = self
        
        txtSenha.isSecureTextEntry = true
        txtConfirmaSenha.isSecureTextEntry = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CadastroViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard(){
        view.endEditing(true)
    }
    
    @IBAction func cadastrarUsuario(_ sender: AnyObject) {
        let nome = textNome.text
        let sobrenome = textSobrenome.text
        let senha = txtSenha.text
        let confirmaSenha = txtConfirmaSenha.text
        let email = txtEmail.text
        
        if ((nome?.isEmpty)! || (sobrenome?.isEmpty)!){
            errorMessage.text = "Nome e Sobrenome são obrigatórios*"
        } else if ((senha?.isEmpty )! || (confirmaSenha?.isEmpty)!){
            errorMessage.text = "A senha de acesso é obrigatória*"
        } else if ((email?.isEmpty)!) {
            errorMessage.text = "Email de acesso é obrigatório*"
        } else {
            errorMessage.text = ""
        }
        
        if (!ValidateUtils.isValidEmail(email: email!)){
            errorMessage.text = "Por favor informe um email válido*"
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let person = Usuario(context: context)
        person.email = email
        person.senha = senha
        person.sobrenome = sobrenome
        person.nome = nome
        
        do{
            try context.save()
        } catch let error as NSError{
            print("Ocorreu um erro ao salvar o modelo")
        }
    }
}
