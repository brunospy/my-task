//
//  ViewController.swift
//  MyTask
//
//  Created by Bruno Ribeiro on 12/18/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var entrarBtn: UIButton!
    @IBOutlet weak var textSenha: UITextField!

    @IBAction func btnEntrar(_ sender: AnyObject) {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        entrarBtn.layer.cornerRadius = 15
            
        textEmail.delegate = self
        textSenha.delegate = self
        textSenha.isSecureTextEntry = true
            
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        LoginService.cadastrarNovoUsuario()
    }
    
    func dismissKeyboard(){
        view.endEditing(true)
    }
    

}

