//
//  Usuario+CoreDataProperties.swift
//  MyTask
//
//  Created by Bruno Ribeiro on 12/30/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import Foundation
import CoreData


extension Usuario {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Usuario> {
        return NSFetchRequest<Usuario>(entityName: "Usuario");
    }

    @NSManaged public var nome: String?
    @NSManaged public var sobrenome: String?
    @NSManaged public var email: String?
    @NSManaged public var senha: String?

}
