//
//  ValidateUtils.swift
//  MyTask
//
//  Created by Bruno Ribeiro on 12/18/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import Foundation

class ValidateUtils {

    class func isValidEmail(email:String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }


}
